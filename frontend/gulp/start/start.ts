import { exec } from "child_process";
import { series } from "gulp";
import { deRedux } from "../../../app/gulpTasks/automate/deRedux";

export function startDev() {
    return exec("npm run build:ts:dev");
}

export const runDev = series(deRedux, startDev);
