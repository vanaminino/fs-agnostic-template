import { Store, Unsubscribe } from "redux";
import { ICb, IStoreService } from "../../../app/types";

export class StoreService implements IStoreService {
  constructor(private store: Store) { }

  public dispatch(action: any): any {
    this.store.dispatch({ ...action });
  }

  public subscribe(cb: (state: AppState) => void): Unsubscribe {
    return this.store.subscribe(() => cb(this.getState()));
  }

  public getState(): any {
    return this.store.getState();
  }
}
