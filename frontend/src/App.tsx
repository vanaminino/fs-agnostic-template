import * as React from "react";
import { HealthCheck } from "./healthCheckStatus/healthCheck.component";

export const App: React.FunctionComponent<{}> = () => {
    return (
        <div>
            <h1>{"Full-stack TS Sample"}</h1>
            <HealthCheck />
        </div>
    );
};
