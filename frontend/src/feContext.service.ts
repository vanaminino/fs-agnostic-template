import { createStore, Store } from "redux";

import { appReducer } from "../../app/App.reducer";
import { IFEContext } from "../../app/feContext";
import { http, IHttpTool, StoreService, WebApiService } from "./services";

export class ContextService implements IFEContext {
    constructor(
        private storeImplementation: Store,
        private webApiImplementation: IHttpTool,
    ) { }

    public get store(): StoreService {
        return new StoreService(this.storeImplementation);
    }

    public get webApi(): WebApiService {
        return new WebApiService(this.webApiImplementation);
    }
}

const store = createStore(appReducer);
export const feContext = new ContextService(store, http);
