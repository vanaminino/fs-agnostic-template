import config from "config";
import { Express } from "express";
import * as http from "http";
import * as https from "https";
import * as os from "os";

export function BootstrapApp(app: Express): void {
    const port = process.env.PORT || config.get("App.port");
    const key = String(config.get("App.ssl.key")).replace(/\\n/g, os.EOL);
    const cert = String(config.get("App.ssl.key")).replace(/\\n/g, os.EOL);

    if (config.get("App.ssl.enabled")) {
        https.createServer(
            {
                cert,
                key,
            }, app).listen(port);
    } else {
        http.createServer(app).listen(port);
    }
}
