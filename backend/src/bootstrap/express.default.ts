import bodyParser from "body-parser";
import compress from "compression";
import config from "config";
import cookieParser from "cookie-parser";
import cors from "cors";
import { Express } from "express";
import expressSession from "express-session";
import helmet from "helmet";
import methodOverride from "method-override";

export function ExpressDefault(app: Express): Express {
    app.use(bodyParser.json({ limit: "10mb" }));
    app.use(bodyParser.urlencoded({ limit: "10mb", extended: true }));
    app.use(cookieParser());
    app.use(compress());
    app.use(methodOverride());
    app.use(helmet());
    app.use(cors({ credentials: true, origin: true }));

    const sessionOptions: expressSession.SessionOptions = {
        cookie: {
            maxAge: config.get("App.session.cookie.max-age"),
        },
        name: config.get("App.session.name"),
        resave: true,
        rolling: true,
        saveUninitialized: false,
        secret: config.get("App.session.secret"),
    };

    app.use(expressSession(sessionOptions));

    return app;
}
