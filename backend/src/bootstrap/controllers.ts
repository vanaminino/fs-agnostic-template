import { Express } from "express";

import {
    apiRouter,
    devRouter,
 } from "../controllers";

export function Controllers(app: Express): Express {
    app.use("/dev", devRouter);
    app.use("/api", apiRouter);

    return app;
}
