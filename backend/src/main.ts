import config from "config";
import express = require("express");

import { BootstrapApp, Controllers, ExpressDefault } from "./bootstrap";

const expressInstance = express();

expressInstance.set("domain", config.get("App.domain"));
expressInstance.set("port", config.get("App.port"));

export const app = BootstrapApp(Controllers(ExpressDefault(expressInstance)));
