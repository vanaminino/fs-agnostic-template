import { Router } from "express";
import { healthCheck } from "./routes/healthCheck.routes";

export function devMethods(router: Router): void {
    healthCheck(router);
}
