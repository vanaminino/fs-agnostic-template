import { Request, Response, Router } from "express";
import { healthCheckBEService, IHealthCheck } from "../../../../../app/features/healthCheck";

export function healthCheck(router: Router): void {
    async function checkApp(req: Request, res: Response): Promise<any> {
        const healthCheckStatus: IHealthCheck = healthCheckBEService().getAppStatus();
        return res.status(200).send(healthCheckStatus);
    }

    router.get("/healthCheck", checkApp);
}
