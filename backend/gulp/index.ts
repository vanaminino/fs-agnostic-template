import { buildBe } from "./build/buildBe";
import { buildTs } from "./build/buildTs.dev";
import { buildTs as buildTsProd } from "./build/buildTs.prod";
import { cleanDist } from "./build/cleanDist";
import { serve } from "./serve";
import { start } from "./start";

export {
    buildBe,
    buildTs,
    buildTsProd,
    cleanDist,
    serve,
    start,
};
