import { exec } from "child_process";

export function buildTs() {
    return exec("tsc -p tsconfig.dev.json");
}
