import { series } from "gulp";
import { buildTs } from "./buildTs.prod";
import { cleanDist } from "./cleanDist";

export const buildBe = series(cleanDist, buildTs);
