import { series } from "gulp";
import { buildBe } from "./build/buildBe";
import { serve } from "./serve";

export const start = series(buildBe, serve);
