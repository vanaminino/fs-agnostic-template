// warning! This code was generated by @vlr/type-constructor-gen
// manual changes to this file will be overwritten next time the code is regenerated.
import { Sample1Item } from "./sample1Item.type";
import { Sample1Model } from "./sample1.model";

// methods for Sample1Model
export function sample1Model(name: string, item: Sample1Item): Sample1Model {
  return {
    name,
    item,
  };
}
