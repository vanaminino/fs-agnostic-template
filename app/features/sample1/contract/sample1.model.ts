import { Sample1Item } from "./sample1Item.type";

export interface Sample1Model {
  name: string;
  item: Sample1Item;
}
