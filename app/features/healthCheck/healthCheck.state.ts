export interface HealthCheckState {
    isRunning: boolean;
    statusText: string;
}
