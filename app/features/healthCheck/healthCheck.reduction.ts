import { HealthCheckState } from "./healthCheck.state";
import { IHealthCheck } from "./healthCheck.type";

export function updateHealthCheckStatus(prevState: HealthCheckState, model: IHealthCheck): HealthCheckState {
    return {
        ...prevState,
        ...model,
    };
}
