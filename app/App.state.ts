import { HealthCheckState } from "./features/healthCheck/healthCheck.state";

export interface AppState {
    healthCheck: HealthCheckState;
}
