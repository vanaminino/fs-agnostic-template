import { AppState } from "../../app/App.state";

export interface IStoreService {
    dispatch: (action: any) => any;
    subscribe: (cb: ICb) => any;
    getState: () => any;
}

export type ICb = (state: AppState) => void;
