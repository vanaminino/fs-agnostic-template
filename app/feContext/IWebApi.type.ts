export interface IWebApi {
    get<T>(url: string, params?: RequestInit): Promise<T>;
    post<T>(url: string, body?: any, params?: RequestInit): Promise<T>;
    put<T>(url: string, body?: any, params?: RequestInit): Promise<T>;
    delete<T>(url: string, params?: RequestInit): Promise<T>;
}
