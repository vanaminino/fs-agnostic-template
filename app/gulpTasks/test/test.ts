import { series } from "gulp";
import { automate } from "../automate";
import { runTests } from "./runTests";

export const test = series(automate, runTests);
