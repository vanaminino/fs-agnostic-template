import { src } from "gulp";
import jest from "gulp-jest";

export function runTests(): NodeJS.ReadWriteStream {
  process.env.NODE_ENV = "test";
  return src("./**/*.spec.ts")
    .pipe(jest());
}
