import { spawn } from "../tools/spawn";

export function codeQuality(): Promise<void> {
  return spawn("tslint", "tslint", "-p", "./");
}
