import { generate, Options } from "de-redux";

const options: Options = {
  path: "../app",
};

export async function deRedux(): Promise<void> {
  return await generate(options);
}
