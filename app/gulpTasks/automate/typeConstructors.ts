import { changeFileType, transform } from "@vlr/gulp-transform";
import { generate, GeneratorOptions } from "@vlr/type-constructor-gen";
import { dest, src } from "gulp";
import { features, types } from "./files";

export function typeConstructors(): NodeJS.ReadWriteStream {
  const options: GeneratorOptions = {
    quotes: "\"",
    linefeed: "\n",
  };

  return src(types)
    .pipe(transform((file) => ({
      ...file,
      contents: generate(file.contents, file.name, options),
      name: changeFileType(file.name, ".ctor"),
    })))
    .pipe(dest(features));
}
