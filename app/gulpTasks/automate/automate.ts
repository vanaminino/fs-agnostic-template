import { parallel, series } from "gulp";
import { buckets } from "./buckets";
import { deRedux } from "./deRedux";
import { typeConstructors } from "./typeConstructors";

export const automate = parallel(
  series(typeConstructors, buckets),
  deRedux
);
