export const features = "./features";
export const types = ["./features/**/*.type.ts", "./features/**/*.model.ts"];
export const typesAndCtors = [...types, "./features/**/*.ctor.ts"];
