import { generateBuckets } from "@vlr/bucket-gen";
import { transformRange } from "@vlr/gulp-transform";
import { GeneratorOptions } from "@vlr/razor/export";
import { dest, src } from "gulp";
import { features, typesAndCtors } from "./files";

export function buckets(): NodeJS.ReadWriteStream {
  const options: GeneratorOptions = {
    quotes: "\"",
    linefeed: "\n",
  };

  return src(typesAndCtors)
    .pipe(transformRange((files) => generateBuckets(files, options)))
    .pipe(dest(features));
}
